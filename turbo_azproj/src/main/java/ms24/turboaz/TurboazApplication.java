package ms24.turboaz;

import az.ingress.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import ms24.turboaz.repo.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = {"ms24.turboaz", "az.ingress.jwt"})

public class TurboazApplication implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(TurboazApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}

}
