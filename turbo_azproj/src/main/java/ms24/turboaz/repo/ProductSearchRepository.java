package ms24.turboaz.repo;

import io.micrometer.common.util.StringUtils;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.ProductSearchDto;
import ms24.turboaz.entity.Brand;
import ms24.turboaz.entity.Model;
import ms24.turboaz.entity.Product;
import ms24.turboaz.entity.ProductDetails;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class ProductSearchRepository {

    private final EntityManager entityManager;

    public List<Product> search(ProductSearchDto dto) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> query = criteriaBuilder.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);
        Predicate predicates = criteriaBuilder.conjunction();

        if (dto.getMinPrice() != 0 && dto.getMaxPrice() != 0) {
            predicates = criteriaBuilder.and(predicates,
                    criteriaBuilder.between(root.get("price"), dto.getMinPrice(), dto.getMaxPrice()));
        } else if (dto.getMinPrice() != 0) {
            predicates = criteriaBuilder.and(predicates,
                    criteriaBuilder.greaterThanOrEqualTo(root.get("price"), dto.getMinPrice()));
        } else if (dto.getMaxPrice() != 0) {
            predicates = criteriaBuilder.and(predicates,
                    criteriaBuilder.lessThanOrEqualTo(root.get("price"), dto.getMaxPrice()));
        }
        if (!StringUtils.isEmpty(dto.getCity())) {
            Join<Product, ProductDetails> productDetailsJoin = root.join("productDetails", JoinType.INNER);
            predicates = criteriaBuilder.and(predicates,
                    criteriaBuilder.equal(productDetailsJoin.get("city"), dto.getCity()));
        }

         if (!StringUtils.isEmpty(dto.getBrandName())) {
        Join<Product, Model> modelJoin = root.join("model", JoinType.INNER);
        Join<Model, Brand> brandJoin = modelJoin.join("brand", JoinType.INNER);
        predicates = criteriaBuilder.and(predicates,
                criteriaBuilder.equal(brandJoin.get("brandName"), dto.getBrandName()));
    }

        if (!StringUtils.isEmpty(dto.getModelName())) {
            Join<Product, Model> modelJoin = root.join("model", JoinType.INNER);
            predicates = criteriaBuilder.and(predicates,
                    criteriaBuilder.equal(modelJoin.get("modelName"), dto.getModelName()));
        }
        query.where(predicates);
        return entityManager.createQuery(query).getResultList();

    }

}

