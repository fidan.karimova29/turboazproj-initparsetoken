package ms24.turboaz.service.impl;


import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.*;
import ms24.turboaz.entity.ProductDetails;
import ms24.turboaz.repo.ProductDetailsRepository;
import ms24.turboaz.service.interfaces.ProductDetailsService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductDetailsServiceImpl implements ProductDetailsService {
    private final ModelMapper modelMapper;
    private final ProductDetailsRepository productDetailsRepository;

    @Override
    public ProductDetailsResponseDto create(ProductDetailsRequestDto productDetailsRequestDto) {
        ProductDetails productDetails = modelMapper.map(productDetailsRequestDto, ProductDetails.class);
        ProductDetails productInRepo = productDetailsRepository.save(productDetails);
        return modelMapper.map(productInRepo, ProductDetailsResponseDto.class);
    }

    @Override
    public ProductDetailsResponseDto update(Long id, ProductDetailsRequestDto productDetailsRequestDto) {
        ProductDetails productDetails = productDetailsRepository.findById(id).orElseThrow(()
                -> new RuntimeException("productDet is not found with id " + id));
        productDetails.setTypeOfBan(productDetailsRequestDto.getTypeOfBan());
        productDetails.setCity(productDetailsRequestDto.getCity());
        productDetails.setGraduationYear(productDetailsRequestDto.getGraduationYear());
        productDetails.setColor(productDetailsRequestDto.getColor());
        productDetails.setEngine(productDetailsRequestDto.getEngine());
        productDetails.setMarch(productDetailsRequestDto.getMarch());
        productDetails.setGearBox(productDetailsRequestDto.getGearBox());
        productDetails.setNewOrNot(productDetailsRequestDto.isNewOrNot());
        productDetails.setVinCode(productDetailsRequestDto.getVinCode());
        productDetails.setCountryIsCreated(productDetailsRequestDto.getCountryIsCreated());
        productDetails.setGear(productDetailsRequestDto.getGear());
        productDetails.setFuelType(productDetailsRequestDto.getFuelType());
        productDetails.setCountOfOwners(productDetailsRequestDto.getCountOfOwners());
        productDetails.setCreditOrBarter(productDetailsRequestDto.getCreditOrBarter());
        productDetails.setState(productDetailsRequestDto.getState());
        productDetailsRepository.save(productDetails);
        //  modelMapper.map(productDetailsRequestDto, productDetails);
        // productDetailsRepository.save(productDetails);
        return modelMapper.map(productDetails, ProductDetailsResponseDto.class);
    }


    @Override
    public void delete(Long id) {
        ProductDetails productDetails = productDetailsRepository.findById(id).orElseThrow(()
                -> new RuntimeException("proddet is not found with id " + id));
        productDetailsRepository.delete(productDetails);
    }

    @Override
    public ProductDetailsResponseDto getElementById(Long id) {
        ProductDetails productDetails = productDetailsRepository.findById(id).orElseThrow(()
                -> new RuntimeException("proddet is not found with id " + id));
        return modelMapper.map(productDetails, ProductDetailsResponseDto.class);
    }

    @Override
    public List<ProductDetailsResponseDto> getAllProductDetails() {
        List<ProductDetails> all = productDetailsRepository.findAll();
        return all.stream().map(prodDet->
                modelMapper.map(prodDet,ProductDetailsResponseDto.class)).toList();
    }
}
