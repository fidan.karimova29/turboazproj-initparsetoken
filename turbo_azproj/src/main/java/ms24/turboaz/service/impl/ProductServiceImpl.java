package ms24.turboaz.service.impl;
import jakarta.persistence.criteria.*;

import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.*;
import ms24.turboaz.entity.*;
import ms24.turboaz.repo.*;
import ms24.turboaz.service.interfaces.ProductService;
import org.hibernate.query.Page;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ModelMapper modelMapper;
    private final ProductRepository productRepository;
    private final BrandRepository brandRepository;
    private final OwnerRepository ownerRepository;
    private final ModelRepository modelRepository;
    private final ImageRepository imageRepository;
    private final ProductDetailsRepository productDetailsRepository;
    @Override
    public ProductResponseDto create(ProductRequestDto productRequestDto) {
        Product product = modelMapper.map(productRequestDto,Product.class);
       // Brand brand = findBrandById(productRequestDto.getBrand_id());
        Owner owner = findOwnerById(productRequestDto.getOwner_id());
        Model model = findModelById(productRequestDto.getModel_id());
       // Image images = findImagesById(productRequestDto.getImage_id());
        ProductDetails productDetails = findProductDetailsById(productRequestDto.getProductDetails_id());
        product.setOwner(owner);
        product.setModel(model);
        //product.setBrand(brand);
        product.setProductDetails(productDetails);

        Product prodInRepo = productRepository.save(product);
        return modelMapper.map(prodInRepo, ProductResponseDto.class);
    }
    private Brand findBrandById(Long id){
        Brand brand = brandRepository.findById(id).orElseThrow(()
                -> new RuntimeException("brand is not found with id " + id));
        return brand;
    }
private Model findModelById(Long id){
    Model model = modelRepository.findById(id).orElseThrow(()
                -> new RuntimeException("brand is not found with id " + id));
        return model;
    }
    private Owner findOwnerById(Long id){
        Owner owner = ownerRepository.findById(id).orElseThrow(()
                -> new RuntimeException("owner is not found with id " + id));
        return owner;
    }
    private ProductDetails findProductDetailsById(Long id){
        ProductDetails productDetails = productDetailsRepository.findById(id).orElseThrow(()
                -> new RuntimeException("productDetails is not found with id " + id));
        return productDetails;
    }
//    private Image findImagesById(Long id){
//        Image image = imageRepository.findById(id).orElseThrow(()
//                -> new RuntimeException("image is not found with id " + id));
//        return image;
//    }

    @Override
    public ProductResponseDto update(Long id, ProductRequestDto productRequestDto) {
        Product product = productRepository.findById(id).orElseThrow(()
                -> new RuntimeException("productis not found with id " + id));
        product.setName(productRequestDto.getName());
        product.setDescription(productRequestDto.getDescription());
        product.setCreationDateTime(LocalDateTime.now());
        product.setPrice(productRequestDto.getPrice());
        return modelMapper.map(product, ProductResponseDto.class);
    }

    @Override
    public void delete(Long id) {
        Product product = productRepository.findById(id).orElseThrow(()
                -> new RuntimeException("product is not found with id " + id));
        productRepository.delete(product);
    }

    @Override
    public ProductResponseDto getElementById(Long id) {
        Product product = productRepository.findById(id).orElseThrow(()
                -> new RuntimeException("product is not found with id " + id));
        return modelMapper.map(product, ProductResponseDto.class);
    }

    @Override
    public List<ProductResponseDto> getAllProducts() {
        List<Product> all = productRepository.findAll();
        return all.stream().map(prod->
                modelMapper.map(prod, ProductResponseDto.class)).toList();
    }




}
