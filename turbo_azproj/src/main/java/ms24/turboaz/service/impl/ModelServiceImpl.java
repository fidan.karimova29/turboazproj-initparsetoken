package ms24.turboaz.service.impl;


import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.BrandDto;
import ms24.turboaz.dto.ImageDto;
import ms24.turboaz.dto.ModelDto;
import ms24.turboaz.dto.ModelRequestDto;
import ms24.turboaz.entity.Brand;
import ms24.turboaz.entity.Image;
import ms24.turboaz.entity.Model;
import ms24.turboaz.entity.ProductDetails;
import ms24.turboaz.repo.BrandRepository;
import ms24.turboaz.repo.ModelRepository;
import ms24.turboaz.repo.OwnerRepository;
import ms24.turboaz.service.interfaces.ModelService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ModelServiceImpl implements ModelService {

    private final ModelMapper modelMapper;
    private final ModelRepository modelRepository;
    private final BrandRepository brandRepository;

    @Override
    public ModelDto create(ModelRequestDto modelRequestDto) {
        Model model = modelMapper.map(modelRequestDto, Model.class);
        Brand brand = findBrandById(modelRequestDto.getBrand_id());
        model.setBrand(brand);
        Model modelInRepo = modelRepository.save(model);
        return modelMapper.map(modelInRepo, ModelDto.class);
    }
    private Brand findBrandById(Long id){
        Brand brand = brandRepository.findById(id).orElseThrow(()
                -> new RuntimeException("brand is not found with id " + id));
        return brand;
    }

    @Override
    public ModelDto update(Long id, ModelRequestDto modelRequestDto) {
        Model model = modelRepository.findById(id).orElseThrow(()
                -> new RuntimeException("model is not found with id " + id));
//        model.setId(modelRequestDto.getId());
//        model.setModelName(modelRequestDto.getModelName());
        modelMapper.map(modelRequestDto, Model.class);
        return modelMapper.map(model,ModelDto.class);
    }

    @Override
    public void delete(Long id) {
        Model model = modelRepository.findById(id).orElseThrow(()
                -> new RuntimeException("model is not found with id " + id));
        modelRepository.delete(model);
    }

    @Override
    public ModelDto getElementById(Long id) {
        Model model = modelRepository.findById(id).orElseThrow(()
                -> new RuntimeException(" model not found with id " + id));
        return modelMapper.map(model, ModelDto.class);
    }

    @Override
    public List<ModelDto> getAllModels() {
        List<Model> all = modelRepository.findAll();
        return all.stream().map(model->
                modelMapper.map(model,ModelDto.class)).toList();
    }
}
