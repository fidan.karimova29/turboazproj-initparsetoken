package ms24.turboaz.service.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;
import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.*;
import ms24.turboaz.entity.Brand;
import ms24.turboaz.entity.Image;
import ms24.turboaz.entity.Owner;
import ms24.turboaz.entity.ProductDetails;
import ms24.turboaz.repo.OwnerRepository;
import ms24.turboaz.service.interfaces.OwnerService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class OwnerServiceImpl implements OwnerService {
    private final ModelMapper modelMapper;
    private final OwnerRepository ownerRepository;

    @Override
    public OwnerDto create(OwnerRequestDto ownerRequestDto) {
        Owner owner = modelMapper.map(ownerRequestDto, Owner.class);
        Owner ownerInRepo = ownerRepository.save(owner);
        return modelMapper.map(ownerInRepo, OwnerDto.class);
    }

    @Override
    public OwnerDto update(Long id, OwnerRequestDto ownerRequestDto) {
        Owner ownerEntity = ownerRepository.findById(id).orElseThrow(()
                -> new RuntimeException("owner is not found with id " + id));
        ownerEntity.setOwnerName(ownerRequestDto.getOwnerName());
        ownerEntity.setSurname(ownerRequestDto.getSurname());
        ownerEntity.setPhoneNumber(ownerRequestDto.getPhoneNumber());
       // modelMapper.map(ownerRequestDto, ownerEntity);
        Owner updatedOwnerEntity = ownerRepository.save(ownerEntity);
        return modelMapper.map(updatedOwnerEntity,OwnerDto.class);
    }

    @Override
    public void delete(Long id) {
        Owner ownerEntity = ownerRepository.findById(id).orElseThrow(()
                -> new RuntimeException("owner is not found with id " + id));
        ownerRepository.delete(ownerEntity);

    }

    @Override
    public OwnerDto getElementById(Long id) {
        Owner ownerEntity = ownerRepository.findById(id).orElseThrow(()
                -> new RuntimeException("owner is not found with id " + id));
        return modelMapper.map(ownerEntity, OwnerDto.class);
    }

    @Override
    public List<OwnerDto> getAllOwners() {
        List<Owner> all = ownerRepository.findAll();
        return all.stream().map(owner->
                modelMapper.map(owner,OwnerDto.class)).toList();
    }
}
