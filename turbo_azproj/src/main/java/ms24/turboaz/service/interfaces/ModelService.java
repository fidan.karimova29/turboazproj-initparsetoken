package ms24.turboaz.service.interfaces;

import ms24.turboaz.dto.ImageDto;
import ms24.turboaz.dto.ImageRequestDto;
import ms24.turboaz.dto.ModelDto;
import ms24.turboaz.dto.ModelRequestDto;

import java.util.List;

public interface ModelService {
    ModelDto create(ModelRequestDto modelRequestDto);
    ModelDto update(Long id,ModelRequestDto modelRequestDto);
    void delete(Long id);
    ModelDto getElementById(Long id);
    List<ModelDto> getAllModels();
}
