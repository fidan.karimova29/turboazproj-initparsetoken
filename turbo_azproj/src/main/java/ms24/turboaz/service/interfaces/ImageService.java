package ms24.turboaz.service.interfaces;

import ms24.turboaz.dto.BrandDto;
import ms24.turboaz.dto.BrandRequestDto;
import ms24.turboaz.dto.ImageDto;
import ms24.turboaz.dto.ImageRequestDto;

import java.util.List;

public interface ImageService {
    ImageDto create(ImageRequestDto imageRequestDto);
    ImageDto update(Long id,ImageRequestDto imageRequestDto);
    void delete(Long id);
    ImageDto getElementById(Long id);
    List<ImageDto> getAllImages();
}
