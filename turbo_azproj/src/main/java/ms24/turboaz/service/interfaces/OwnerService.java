package ms24.turboaz.service.interfaces;

import ms24.turboaz.dto.ModelDto;
import ms24.turboaz.dto.ModelRequestDto;
import ms24.turboaz.dto.OwnerDto;
import ms24.turboaz.dto.OwnerRequestDto;
import ms24.turboaz.entity.Owner;

import java.util.List;

public interface OwnerService {
    OwnerDto create(OwnerRequestDto ownerRequestDto);
    OwnerDto update(Long id,OwnerRequestDto ownerRequestDto);
    void delete(Long id);
    OwnerDto getElementById(Long id);
    List<OwnerDto> getAllOwners();
}
