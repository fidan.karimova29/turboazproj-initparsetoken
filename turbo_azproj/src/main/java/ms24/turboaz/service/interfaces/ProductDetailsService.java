package ms24.turboaz.service.interfaces;

import ms24.turboaz.dto.*;

import java.util.List;

public interface ProductDetailsService {
    ProductDetailsResponseDto  create(ProductDetailsRequestDto productDetailsRequestDto);
    ProductDetailsResponseDto update(Long id,ProductDetailsRequestDto productDetailsRequestDto);
    void delete(Long id);
    ProductDetailsResponseDto getElementById(Long id);
    List<ProductDetailsResponseDto> getAllProductDetails();
}
