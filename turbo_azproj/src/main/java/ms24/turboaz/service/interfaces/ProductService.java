package ms24.turboaz.service.interfaces;

import ms24.turboaz.dto.ProductDto;
import ms24.turboaz.dto.ProductRequestDto;
import ms24.turboaz.dto.ProductResponseDto;
import ms24.turboaz.dto.ProductSearchDto;
import org.hibernate.query.Page;

import java.awt.print.Pageable;
import java.util.List;

public interface ProductService {
    ProductResponseDto create(ProductRequestDto productRequestDto);
    ProductResponseDto update(Long id,ProductRequestDto productRequestDto);
    void delete(Long id);
    ProductResponseDto getElementById(Long id);
    List<ProductResponseDto> getAllProducts();

}
