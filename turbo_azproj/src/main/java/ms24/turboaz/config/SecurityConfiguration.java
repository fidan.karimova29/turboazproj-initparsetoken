package ms24.turboaz.config;

import az.ingress.jwt.config.JwtAuthFilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final JwtAuthFilterConfigurerAdapter jwtAuthFilterConfigurerAdapter;

    @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(requests ->
                        requests.requestMatchers("/user/public").permitAll()
                                .anyRequest().authenticated())
                .formLogin(Customizer.withDefaults());
        http.apply(jwtAuthFilterConfigurerAdapter);
        return http.build();
    }

}
