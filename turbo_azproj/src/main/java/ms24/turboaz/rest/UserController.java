package ms24.turboaz.rest;

import az.ingress.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final JwtService jwtService;
    private final String BEARER = "Bearer";




    @GetMapping("/authenticated")
    public String sayHelloAuthenticated(Authentication authentication) {
        return jwtService.issueToken(authentication);
    }
    @GetMapping("/public")
    public String sayHello(@RequestHeader("authorization") String authorizationHeader) {
        final String jwt = authorizationHeader.substring(BEARER.length())
                .trim();
        System.out.println(jwt);
        return "parsed token " + jwtService.parseToken(jwt);
    }

}
