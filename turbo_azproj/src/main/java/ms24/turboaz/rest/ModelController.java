package ms24.turboaz.rest;

import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.ModelDto;
import ms24.turboaz.dto.ModelRequestDto;
import ms24.turboaz.service.impl.ModelServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/model")
@RequiredArgsConstructor
public class ModelController {
    private final ModelServiceImpl modelServiceImpl;



    @PostMapping
    public ModelDto create(@RequestBody ModelRequestDto modelRequestDto) {
        return modelServiceImpl. create(modelRequestDto);
    }

    @PutMapping("/{id}")

    public ModelDto update(@PathVariable Long id, @RequestBody ModelRequestDto modelRequestDto) {
        return modelServiceImpl.update(id, modelRequestDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        modelServiceImpl.delete(id);
    }

    @GetMapping("/{id}")
    public ModelDto getElementById(@PathVariable Long id) {
        return modelServiceImpl.getElementById(id);
    }
    @GetMapping
    List<ModelDto> getAllModels() {
        return modelServiceImpl.getAllModels();
    }
}
