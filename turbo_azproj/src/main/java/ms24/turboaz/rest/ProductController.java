package ms24.turboaz.rest;

import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.*;
import ms24.turboaz.entity.Product;
import ms24.turboaz.repo.ProductSearchRepository;
import ms24.turboaz.service.impl.ProductServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductServiceImpl productServiceImpl;
    private final ProductSearchRepository productSearchRepository;

    @PostMapping
    public ProductResponseDto create(@RequestBody ProductRequestDto productRequestDto) {
        return productServiceImpl.create(productRequestDto);
    }

    @PutMapping("/{id}")
    public ProductResponseDto update(@PathVariable Long id,
                                     @RequestBody ProductRequestDto productRequestDto) {
        return productServiceImpl.update(id, productRequestDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        productServiceImpl.delete(id);
    }

    @GetMapping("/{id}")
    public ProductResponseDto getElementById(@PathVariable Long id) {
        return productServiceImpl.getElementById(id);
    }
    @GetMapping
    List<ProductResponseDto> getAllProducts() {
        return productServiceImpl.getAllProducts();
    }


@GetMapping("/search")
public List<Product> searchProducts(@RequestBody ProductSearchDto productSearchDto) {

    return productSearchRepository.search(productSearchDto);
}
}
