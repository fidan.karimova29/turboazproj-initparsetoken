package ms24.turboaz.rest;

import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.BrandDto;
import ms24.turboaz.dto.BrandRequestDto;
import ms24.turboaz.service.impl.BrandServiceImpl;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/brands")
@RequiredArgsConstructor
public class BrandController {
    private final BrandServiceImpl brandServiceImpl;

    @PostMapping
    public BrandDto create(@RequestBody BrandRequestDto brandRequestDto) {
        return brandServiceImpl.create(brandRequestDto);
    }

    @PutMapping("/{id}")
    public BrandDto update(@PathVariable Long id, @RequestBody BrandRequestDto brandRequestDto) {
        return brandServiceImpl.update(id, brandRequestDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        brandServiceImpl.delete(id);
    }

    @GetMapping("/{id}")
    public BrandDto getElementById(@PathVariable Long id) {
        return brandServiceImpl.getElementById(id);
    }
    @GetMapping
    List<BrandDto> getAllBrands() {
        return brandServiceImpl.getAllBrands();
    }
}
