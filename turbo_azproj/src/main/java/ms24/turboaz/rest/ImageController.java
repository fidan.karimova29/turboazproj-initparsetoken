package ms24.turboaz.rest;

import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.ImageDto;
import ms24.turboaz.dto.ImageRequestDto;
import ms24.turboaz.dto.ModelDto;
import ms24.turboaz.dto.ModelRequestDto;
import ms24.turboaz.service.impl.ImageServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/image")
@RequiredArgsConstructor
public class ImageController {
    private  final ImageServiceImpl imageServiceImpl;
    @PostMapping
    public ImageDto create(@RequestBody ImageRequestDto imageRequestDto) {
        return imageServiceImpl. create(imageRequestDto);
    }

    @PutMapping("/{id}")

    public ImageDto update(@PathVariable Long id, @RequestBody ImageRequestDto imageRequestDto) {
        return imageServiceImpl.update(id, imageRequestDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        imageServiceImpl.delete(id);
    }

    @GetMapping("/{id}")
    public ImageDto getElementById(@PathVariable Long id) {
        return imageServiceImpl.getElementById(id);
    }
    @GetMapping
    List<ImageDto> getAllModels() {
        return imageServiceImpl.getAllImages();
    }
}
