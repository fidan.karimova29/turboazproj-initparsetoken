package ms24.turboaz.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import ms24.turboaz.dto.enums.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class ProductDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String typeOfBan;
    private String city;
    private Integer graduationYear;
    private String  color;
    private String  engine;
    private String march;
    private String gearBox;
    private boolean newOrNot;
    private String vinCode;
    private String countryIsCreated;
    private Long countOfView;
   // private boolean coloredOrNot;

    @Enumerated(EnumType.STRING)
    private Gear gear ;

    @Enumerated(EnumType.STRING)
    private FuelType fuelType ;


    @Enumerated(EnumType.STRING)
    private CountOfOwners countOfOwners;

    @Enumerated(EnumType.STRING)
    private CreditOrBarter creditOrBarter;

    @Enumerated(EnumType.STRING)
    private State state;




}
