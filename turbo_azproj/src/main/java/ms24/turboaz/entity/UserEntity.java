package ms24.turboaz.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
@Getter
@Builder
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor


public class UserEntity implements UserDetails {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @OneToMany
        private List<UserAuthority> authorities;

        private String password;

        private String username;

        private boolean accountNonExpired;

        private boolean accountNonLocked;

        private boolean credentialsNonExpired;

        private boolean enabled;
    }


