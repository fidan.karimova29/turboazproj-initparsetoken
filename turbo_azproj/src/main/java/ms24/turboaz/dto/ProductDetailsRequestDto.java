package ms24.turboaz.dto;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.dto.enums.*;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailsRequestDto {
    private Long id;
    private String typeOfBan;
    private String city;
    private Integer graduationYear;
    private String  color;
    private String engine;
    private String march;
    private String gearBox;
    private boolean newOrNot;
    private String vinCode;
    private Long countOfView;
    private String countryIsCreated;
 //   private boolean coloredOrNot;

    @Enumerated(EnumType.STRING)
    private Gear gear ;

    @Enumerated(EnumType.STRING)
    private FuelType fuelType ;


    @Enumerated(EnumType.STRING)
    private CountOfOwners countOfOwners;

    @Enumerated(EnumType.STRING)
    private CreditOrBarter creditOrBarter;

    @Enumerated(EnumType.STRING)
    private State state;
}
