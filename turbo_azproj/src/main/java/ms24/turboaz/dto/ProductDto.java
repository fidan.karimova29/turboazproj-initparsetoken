package ms24.turboaz.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.dto.enums.*;
import ms24.turboaz.entity.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private Long id;
    private String name;
    private LocalDateTime creationDateTime;
    private String description;
    private double price;
    // private Brand brand;
    private Model model;
    private List<Image> images;
    private Owner owner;
    private ProductDetails productDetails;
    private Long image_id;
    private Long owner_id;
   // private Long brand_id;
    private Long model_id;
    private Long productDetails_id;






}
