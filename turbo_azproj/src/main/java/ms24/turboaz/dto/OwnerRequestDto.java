package ms24.turboaz.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OwnerRequestDto {
    private Long id;
    private String ownerName;
    private String surname;
    @Pattern(regexp = "^(055|051|050|070|099|077)\\d{7}$", message = "Phone number must start with 055, 051, or 050 and have 10 digits")
    @NotNull
    @NotBlank(message = "Phone number is required")
    private String phoneNumber;
   // private Long countOfProducts = 0L;
}
