package ms24.turboaz.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.entity.Brand;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModelRequestDto {
    private Long id;
    private String modelName;
   // private Brand brand;
    private Long brand_id;

}
