package ms24.turboaz.dto.enums;

public enum Gear {
    front,
    back,
    full
}
