package ms24.turboaz.dto.enums;

public enum CreditOrBarter {

    credit,
    barter;
}
