package ms24.turboaz.dto.enums;

public enum FuelType {
    benzin,
    dizel,
    gas,
    electro,
    hybrid

}
