package ms24.turboaz.dto;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.dto.enums.*;
import ms24.turboaz.entity.Brand;
import ms24.turboaz.entity.Image;
import ms24.turboaz.entity.Owner;
import ms24.turboaz.entity.ProductDetails;

import java.time.LocalDateTime;
import java.util.List;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestDto {
    private Long id;
    private String name;
    private LocalDateTime creationDateTime;
    private String description;
    private double price;
    private Long owner_id;
   //private Long brand_id;
   private Long model_id;
    private Long productDetails_id;

}
