package ms24.turboaz.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.entity.Brand;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModelDto {
    private Long id;
    private String modelName;
    private Brand brand;
   // private Long brand_id;
}
