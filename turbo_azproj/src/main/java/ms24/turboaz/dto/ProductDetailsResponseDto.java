package ms24.turboaz.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.dto.enums.CountOfOwners;
import ms24.turboaz.dto.enums.FuelType;
import ms24.turboaz.dto.enums.Gear;

import java.time.LocalDateTime;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailsResponseDto {
    private Long id;
    private LocalDateTime creationDateTime;
    private String typeOfBan;
    private String city;
    private Integer graduationYear;
    private String  color;
    private String  engine;
    private String march;
    private String gearBox;
    private Gear gear ;
    private boolean newOrNot;
    private String countryIsCreated;
    private Long countOfView;
 //   private boolean coloredOrNot;
    private FuelType fuelType ;
    private CountOfOwners countOfOwners;

}
