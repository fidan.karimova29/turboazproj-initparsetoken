package ms24.turboaz.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.entity.Brand;
import ms24.turboaz.entity.Model;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BrandDto {
    private Long id;
    private String brandName;
   // private List<Model> models;
    //private Long model_id;
}
